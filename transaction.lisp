(in-package :cl-at-sign)

(export '(with-transaction save-doc delete-doc save-doc-list-with-example))

(defvar *in-transaction* nil)
(defvar *documents-in-transaction* nil)

(defgeneric delete-doc (doc &rest args))


(defmethod save-doc :around ((doc doc) &rest args)
  (declare (ignore args))
  (if *in-transaction*
      (push doc *documents-in-transaction*)
      (call-next-method)))

(defmethod save-doc ((null null) &rest args)
  (declare (ignore null args)))

(defmethod save-doc ((list list) &rest args)
  (apply #'save-doc-list-with-example (car list) args))

(defgeneric save-doc-list-with-example (example list &rest args)
  (:documentation "Save a list of docs, with example provided for
  class dispatching. This method is called by `save-doc' if called on
  a list."))

(defmacro with-transaction ((db &key server) &body body)
  `(let ((*in-transaction* t)
	 *documents-in-transaction*)
      ,@body
      (save-doc (nreverse *documents-in-transaction*) ',db ,server)))

;; example:
;; (with-transaction (test)
;;   (let ((doc1 (make-doc-instance :foo 1))
;; 	(doc2 (make-doc-instance :foo 2))
;; 	(doc3 (make-doc-instance :foo 3)))
;;     (save-doc doc1)
;;     (save-doc doc2)
;;     (save-doc doc3)

;;     (format t "~s ~s ~s~%" doc1 doc2 doc2)))


;; Copyright (C) 2008
;; Ryszard Szopa <ryszard.szopa@gmail.com> 

;; This software is provided 'as-is', without any express or implied
;; warranty. In no event will the authors be held liable for any
;; damages arising from the use of this software.

;; Permission is granted to anyone to use this software for any
;; purpose, including commercial applications, and to alter it and
;; redistribute it freely, subject to the following restrictions:

;; 1. The origin of this software must not be misrepresented; you must
;;    not claim that you wrote the original software. If you use this
;;    software in a product, an acknowledgment in the product
;;    documentation would be appreciated but is not required.

;; 2. Altered source versions must be plainly marked as such, and must
;;    not be misrepresented as being the original software.

;; 3. This notice may not be removed or altered from any source
;;    distribution.