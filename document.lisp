(in-package :cl-at-sign)

(export '(@ doc aget id-of rev-of get-doc-from-id make-doc save-doc attributes-of))

(defun make-alist ()
  '((nil)))

(defclass* doc ()
  ((attributes (make-alist) :documentation "Alist taken from couchdb"))
  (:documentation "A thin objectish wrapper over an alist returned by
  CouchDB."))

(defmethod print-object ((doc doc) s)
  (format s "#<doc~{ ~s~}>" (alist-plist (attributes-of doc))))

(defmethod (setf aget) (val item (doc doc))
  (let ((atts (attributes-of doc)))
    (setf (aget item atts) val)
    (setf (attributes-of doc) atts)
    val))

(defmethod aget (item (doc doc))
  (aget item (attributes-of doc)))

(defmethod  make-doc ((n (eql nil)) &rest keys &key &allow-other-keys)
  "Make a document whose attributes are like the ones specified by `keys'"
  (make-instance 'doc :attributes (or (plist-alist keys) (make-alist))))

(defmethod json:encode-json ((doc doc) s)
  (json:encode-json (attributes-of doc) s))

(defgeneric save-doc (doc &rest rest)
  (:documentation "Save `doc' into some kind of storage."))

(defmethod validates ((doc doc) &optional (err-on-failure t))
  (declare (ignore doc err-on-failure))
  t)

;; Copyright (C) 2008
;; Ryszard Szopa <ryszard.szopa@gmail.com> 

;; This software is provided 'as-is', without any express or implied
;; warranty. In no event will the authors be held liable for any
;; damages arising from the use of this software.

;; Permission is granted to anyone to use this software for any
;; purpose, including commercial applications, and to alter it and
;; redistribute it freely, subject to the following restrictions:

;; 1. The origin of this software must not be misrepresented; you must
;;    not claim that you wrote the original software. If you use this
;;    software in a product, an acknowledgment in the product
;;    documentation would be appreciated but is not required.

;; 2. Altered source versions must be plainly marked as such, and must
;;    not be misrepresented as being the original software.

;; 3. This notice may not be removed or altered from any source
;;    distribution.
